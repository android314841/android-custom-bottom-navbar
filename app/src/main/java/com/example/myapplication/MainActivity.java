package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView home, settings;

    @Override
    protected void onStart() {
        super.onStart();
        loadFragment(new HomeFragment());
        // Initially tint the home icon to white and settings icon to gray
//        tintImageView(home, Color.WHITE);
//        tintImageView(settings, Color.GRAY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        home = findViewById(R.id.home_ic);
        settings = findViewById(R.id.setting_ic);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Home focused, settings unfocused
                tintImageView(home, Color.WHITE);
                tintImageView(settings, Color.GRAY);
                loadFragment(new SettingsFragment());
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Settings focused, home unfocused
                tintImageView(settings, Color.WHITE);
                tintImageView(home, Color.GRAY);
                loadFragment(new HomeFragment());
            }
        });
    }

    private void tintImageView(ImageView imageView, int tintColor) {
        Drawable drawable = imageView.getDrawable();
        if (drawable != null) {
            drawable.setColorFilter(tintColor, PorterDuff.Mode.SRC_IN);
            imageView.setImageDrawable(drawable);
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentView, fragment);
        transaction.addToBackStack(null); // Add to back stack if needed
        transaction.commit();
    }
}
